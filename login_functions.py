from flask import redirect
from lxml import etree
import os
import urllib.parse
from flask import request

'''
    User authentication configuration
    ---------------------------------
'''

def getGroupAuthenticationLevel(groupname):
    conf = etree.parse('{}/config/authentication_config.xml'.format(os.path.dirname(os.path.realpath(__file__))))
    for grp in conf.xpath("/Config/Authentication/Group"):
        if grp.text.lower() == groupname.lower():
            return int(grp.get('level'))

    return -1

def getUniqueAuthenticationLevel(uniquename):
    conf = etree.parse('{}/config/authentication_config.xml'.format(os.path.dirname(os.path.realpath(__file__))))
    for unique in conf.xpath("/Config/Authentication/Unique"):
        if unique.text.lower() == uniquename.lower():
            return int(unique.get('level'))

    return -1

'''
    LOGIN Functions
    ---------------
'''
def connect_callback(oauth_client=None):
    if oauth_client is None:
        print('ERROR: OAUTH should be used for this webapp')

    level = 1

    details = oauth_client.get('userinfo').json()
    print(details)

    #groups = oauth_client.get('G2').json()
    #print(groups)

    unique_lvl = getUniqueAuthenticationLevel(details['email'])
    if unique_lvl > level:
        level = unique_lvl

    #for grp in groups['groups']:
    #    unique_lvl = getGroupAuthenticationLevel(grp)
    #    if unique_lvl > level:
    #        level = unique_lvl

    return {
            'level': level,
            'email': details['email'],
            'details': details
        }

def disconnect_callback():
    return_to = urllib.parse.urlencode({'redirect_uri': request.url_root.replace('http','https')})
    logout_url = 'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout'
    logout_url += '?' + return_to
    return redirect(logout_url)

def connect():
    return 'Only OAUTH connection is supported for this webapp .. but we could imagine to have a custom form that we return here'