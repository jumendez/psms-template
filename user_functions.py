import json
import os

from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from requests import Session
from zeep import Client
from zeep.transports import Transport
from lxml import etree
import requests

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB
from datetime import datetime as dt

# Template function
def sendInvoice(reqmgt_id):

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    prodDBObj = ProdDB(DBHost,DBUsr,DBPwd,DBPort,DBName)
    reqMgtDf = prodDBObj.getProductionDetails(select=[{
                    'table'    : 'RequestManagement',
                    'column'   : 'Qty'
                },{
                    'table'    : 'Users',
                    'column'   : 'Details',
                    'jsonfield': 'PersonID'
                },{
                    'table'    : 'PurchaseRequests',
                    'column'   : 'Details',
                    'jsonfield': 'budgetCode'
                }],
                filters=[{
                    'table':  'RequestManagement',
                    'column': 'ReqMgt_id',
                    'comp': '=',
                    'val': int(reqmgt_id)
                }])

    qty = reqMgtDf['requestmanagement_qty'].iloc[0]
    clientpid = reqMgtDf['users_details_personid'].iloc[0]
    budgetcode = reqMgtDf['purchaserequests_details_budgetcode'].iloc[0]

    devDf = prodDBObj.getProductionDetails(select=[{
                    'table'    : 'Devices',
                    'column'   : 'Details',
                    'jsonfield': 'sn'
                },{
                    'table'    : 'Devices',
                    'column'   : 'Details',
                    'jsonfield': 'pn'
                },{
                    'table'    : 'Devices',
                    'column'   : 'Details',
                    'jsonfield': 'mac'
                }],
                filters=[{
                    'table':  'Devices',
                    'column': 'ReqMgt_id',
                    'comp': '=',
                    'val': int(reqmgt_id)
                }])

    if len(devDf) > 0:
        description = 'This TID refers to the following IPMCs: \n'
        for index, row in devDf.iterrows():
            description += '{} / {} [MAC: {}] \n'.format(row['devices_details_pn'], row['devices_details_sn'], row['devices_details_mac'])
    else:
        description = ''

    tidNumber = createTID('CERN IPMC mezzanine card', 'CERN IPMC', description, 759363, clientpid, qty, 200, budgetcode, 27361, '03041000')
    prodDBObj.updateJSONData('RequestManagement', 'Details', 'TID', tidNumber, 'RequestManagement.Reqmgt_id = {}'.format(reqmgt_id))

def getInvoiceStatus(reqmgt_id):

    DBHost = os.environ['DB_HOST']
    DBUsr = os.environ['DB_USER']
    DBPwd = os.environ['DB_PASS']
    DBPort = os.environ['DB_PORT']
    DBName = os.environ['DB_DBNAME']

    prodDBObj = ProdDB(DBHost,DBUsr,DBPwd,DBPort,DBName)
    reqMgtDf = prodDBObj.getProductionDetails(select=[{
                    'table'    : 'RequestManagement',
                    'column'   : 'Details',
                    'jsonfield': 'TID'
                }],
                filters=[{
                    'table':  'RequestManagement',
                    'column': 'ReqMgt_id',
                    'comp': '=',
                    'val': int(reqmgt_id)
                }])

    docId = reqMgtDf['requestmanagement_details_tid'].iloc[0]

    return getTIDStatus(docId)

# CERN Functions
def getTIDStatus(docId):
    session = Session()
    session.auth = HTTPBasicAuth(os.environ['LOGIN'], os.environ['PASSWORD'])
    transport_with_basic_auth = Transport(session=session)

    client = Client(
        wsdl='https://edhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
        transport=transport_with_basic_auth
    )

    tidXML = '{}'.format(docId)

    node = client.create_message(client.service, 'getDocumentStatus', docId = docId)

    url="https://edhws.cern.ch/ws/services/EDHDocumentImport"
    headers = {'content-type': 'text/xml'}
    body = etree.tostring(node, pretty_print=True)

    response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['LOGIN'], os.environ['PASSWORD']))

    try:
        root = etree.fromstring(response.content)
        responseXML = root.xpath("/soapenv:Envelope/soapenv:Body/ns:getDocumentStatusResponse/ns:return",
                                    namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})[0].text

        print(responseXML)
        root = etree.fromstring(responseXML.encode('utf-8'))

        status = root.xpath('/documentStatus/status')[0].text
        comment = root.xpath('/documentStatus/comments')[0].text

        if status == 'AUTHORISING':
            color = 'w3-text-orange'
        elif status == 'AUTHORISED':
            color = 'w3-text-green'
        else:
            color = 'w3-text-red'

        return '<span class="{}" title="{}" style="cursor: help;">{}</span>'.format(color, comment, status)

    except Exception as e:
        print(e)

    return 'ERROR'


def createTID(title, moduleName, description, supplierPID, clientPID, qty, price, clientBudgetCode, supplierBudgetCode, procurementCode):
    session = Session()
    session.auth = HTTPBasicAuth(os.environ['LOGIN'], os.environ['PASSWORD'])
    transport_with_basic_auth = Transport(session=session)

    client = Client(
        wsdl='https://edhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
        transport=transport_with_basic_auth
    )

    tidXML = '''
    <document xmlns="http://edh.cern.ch/2008/EDHDocument" xmlns:tid="http://edh.cern.ch/2008/TID" xmlns:person="http://edh.cern.ch/2006/Person" xmlns:pc="http://edh.cern.ch/2016/ProcurementCode" xmlns:act="http://edh.cern.ch/2006/ActivityCode" xmlns:loc="http://edh.cern.ch/2006/Location" xmlns:contract="http://edh.cern.ch/2008/Contract" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument ../../java/cern/edh/WS/schema/EDHDocument-WS.xsd http://edh.cern.ch/2008/TID ../../java/cern/edh/WS/schema/TID-WS.xsd">
        <common>
            <type>EDH/TID</type>
            <description>{}</description>
            <creator>
                <person:pid>{}</person:pid>
            </creator>
        </common>
        <docspecific>
            <tid:tid>
                <tid:createdas>supplier</tid:createdas>
                <tid:client>
                    <person:pid>{}</person:pid>
                </tid:client>
                <tid:supplier>
                    <person:pid>{}</person:pid>
                </tid:supplier>
                <tid:comment/>
                <tid:lineitems>
                    <tid:lineitem>
                        <tid:shortdescription>{}</tid:shortdescription>
                        <tid:longdescription>{}</tid:longdescription>
                        <tid:quantity>{}</tid:quantity>
                        <tid:price>{}</tid:price>
                        <tid:clientbco>
                            <tid:budgetcode>{}</tid:budgetcode>
                        </tid:clientbco>
                        <tid:supplierbco>
                            <tid:budgetcode>{}</tid:budgetcode>
                        </tid:supplierbco>
                        <tid:procurementcode><pc:code>{}</pc:code></tid:procurementcode>
                        <tid:removefrominventory>false</tid:removefrominventory>
                        <tid:enterintoinventory>false</tid:enterintoinventory>
                        <tid:alreadydelivered>false</tid:alreadydelivered>
                        <tid:lifetimeoveroneyear>true</tid:lifetimeoveroneyear>
                    </tid:lineitem>
                </tid:lineitems>
            </tid:tid>
        </docspecific>
    </document>
    '''.format(title, supplierPID, clientPID, supplierPID, moduleName, description, qty, price, clientBudgetCode, supplierBudgetCode, procurementCode)

    importXML = '''
    <import-info xmlns="http://edh.cern.ch/2008/EDHDocument"
        xmlns:bc="http://edh.cern.ch/2008/BudgetCode"
        xmlns:person="http://edh.cern.ch/2006/Person"
        xmlns:unit="http://edh.cern.ch/2006/Unit"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument../../document-import-ws/src/java/cern/edh/WS/schema/EDHDocument-WS.xsd">
            <sendDocument>true</sendDocument>
            <msglines>
                <msgline><msg>This document has been created automatically by the PSMS webapplication</msg></msgline>
            </msglines>
    </import-info>
    '''

    node = client.create_message(client.service, 'importDocument', docXml = tidXML, importInfoXml = importXML)

    url="https://edhws.cern.ch/ws/services/EDHDocumentImport"
    headers = {'content-type': 'text/xml'}
    body = etree.tostring(node, pretty_print=True)

    response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['LOGIN'], os.environ['PASSWORD']))

    try:
        root = etree.fromstring(response.content)
        responseCode = root.xpath("/soapenv:Envelope/soapenv:Body/ns:importDocumentResponse/ns:return",
                                    namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})[0].text

        code, tidNumber = responseCode.split(':')

        if code == 'OK':
            return tidNumber
    except:
        print(response.content)

    return -1