# PSMS Template

PSMS Template repository illustrate the use of the PSMS core for a custom project.

## Outline

<ul>
<li>
    <a href="#how-to">How-To</a>
</li>
<li>
    <a href="#configuration">Configuration</a>
    <ul>
        <li><a href="#general-config">General config</a></li>
        <li><a href="#support-config">Support config</a></li>
        <li><a href="#order-config">Order config</a></li>
    </ul>
</li>
</ul>

## How-to

Installation procedure is detailled in the core readme: <a href="https://gitlab.cern.ch/jumendez/psms-core/blob/master/README.md">here</a>

## Configuration

Each part of the webapplication can be configured using XML files located into the config/ folder, which follow the architecture described below:

- **general_config.xml**: general configuration of the webapplication (Navigation, Authentication and IndexContent)
- **order_config.xml**: describe the devices and order action/flow
- **support_config.xml**: configure the support module (Categories and StatusList)

### General config

The config/general_config.xml file should follow the architecture described below:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Config>
	<Navigation>
		<!-- Navigation content -->
	</Navigation>

    <Authentication>
        <!-- Authentication content -->
    </Authentication>

    <IndexContent>
        <!-- Index content -->
    </IndexContent>
</Config>
```



<ul>

<li>
<p><b>Navigation node<b></p>

The Navigation node contains the list of the links displayed in the user section of the navigation bar. Each Item node is made of 3 attributes:

<ul>
<li><b>level</b>: minimum authentication level required to see the link</li>
<li><b>link</b>: target address (could be an url or just a relative path)</li>
<li><b>icon</b>: font-awesome icon reference (<a href="https://fontawesome.com/icons?d=gallery">gallery</a> - only non-pro icon can be used)</li>
</ul>&nbsp;

Finally, the node text will be the displayed message in the navigation bar.

```xml
<Navigation>
	<Item level="0" link="/example" icon="fa fa-comments">Example</Item>
</Navigation>
```

The following user links are supported by the PSMS core:

<ul>
<li><b>/tickets</b>: list of the opened tickets</li>
<li><b>/faq</b>: F.A.Q webpage</li>
<li><b>/order</b>: Order form</li>
</li>
</ul>






### Support config

The config/support_config.xml file should follow the architecture described below:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Support>
    <ThreadDetails>
        <!-- ThreadDetails content -->
    </ThreadDetails>

    <Categories>
        <!-- Categories content -->
    </Categories>

    <StatusList closedStatusId="2">
        <!-- StatusList content -->
    </StatusList>
</Support>
```

<ul>

<li>
<p><b>Thread details<b></p>

The support threads are created from the e-mails and allow the system to group the subjects.
Therefore, each thread can contains detailled field depending on the project/webapplication.
This is why the list of additional fields can be defined into the ThreadDetails node.

The ThreadDetails node contains the list of the field displayed in the thread creation popup. Each Field node contain the followin attibute:

<ul>
<li><b>name</b>: Key name of the parameter, used as the value reference and should not change when defined.</li>
</ul>&nbsp;

Finally, the node text will be the displayed in the form.

```xml
<ThreadDetails>
    <Field name='example'>Example detail</Field>
</ThreadDetails>
```
</li>

<li>
<p><b>Categories<b></p>

The threads can be classify by categories. It allows filtring them and/or computing some statistics to quantify the support.

The Categories node contains the list of the category supported for the threads. The selection is made via the thread creation popup.
Each Category node contain the followin attibute:

<ul>
<li><b>id</b>: Used in the database to link the thread category to a name.</li>
</ul>&nbsp;

Finally, the node text will be the displayed in the form.

```xml
<Categories>
    <Category id="0">CategoryName</Category>
</Categories>
```
</li>

<li>
<p><b>StatusList<b></p>

Each thread can pass by different status before being marked as closed.

The StatusList node contains the list of status supported for the threads. Ticket status is updated by an operator whenever needed and can be set to one of the value listed.
Each Status node contain the followin attibute:

<ul>
<li><b>id</b>: Used in the database to link the thread status to a name.</li>
</ul>&nbsp;

Finally, the node text will be the displayed in the form.

Additionally, in the `StatusList` node, the `closedStatusId` define the limit between status considered as "open" and "closed".
Therefore, in example below, the "open" and "on-going" status mean that action are required when "closed" and "cancelled" mean that the issue is solved.

```xml
<StatusList closedStatusId="2">
    <Status id='0'>Open</Status>
    <Status id='1'>On-going</Status>
    <Status id='2'>Closed</Status>
    <Status id='3'>Cancelled</Status>
</StatusList>
```
</li>
</ul>






### Order config

The config/order_config.xml file should follow the architecture described below:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<Orders>
    <Introduction>
        <!-- Introduction content -->
    </Introduction>

    <Form>
        <!-- Form content -->
    </Form>

    <EnableDeviceSearcher />

    <OrderFlow>
        <!-- OrderFlow content -->
    </OrderFlow>

    <Packages>
        <!-- Packages content -->
    </Packages>

    <Devices>
        <!-- Devices content -->
    </Devices>
</Orders>
```

<ul>

<li>
<p><b>Introduction<b></p>

The text placed into the Introduction node is displayed below the "Information" title of the /order webpage.
To write HTML code, the `<![CDATA[` .. content .. `]]>` feature can be used.

```xml
<Introduction>
    <![CDATA[
        <p>
            Template ordering webpage .. As it is a template, none of the listed module can be purchased using this system ...
            <p style='font-weight:bold;color:orange'>Special advice: I just wanted to test id :-)</p>
        </p>
    ]]>
</Introduction>
```
</li>

<li>
<p><b>Form<b></p>

The Form node describe the different field that shall be filled-up to order a product. Each field is made of the following attributes

<ul>
<li><b>name</b>: Field name, it will be used as a key reference to get back the value</li>
<li><b>type</b>: The input type can be text, date or list. In case of list, the available options shall be defined.</li>
<li><b>verbose</b>: Define the text that will be displayed on the user page</li>
<li><b>width</b>: Define the input width (min: 1, max: 12 [full width])</li>
</ul>&nbsp;

```xml
<Form>
    <Field name='TextfieldKeyName' type="text" verbose="Text field example" width="4"></Field>
    <Field name='DatefieldKeyName' type="date" verbose="Date field example" width="4"></Field>
    <Field name='ListfieldKeyName' type="list" verbose="List field example" width="4">
        <option value="opt_value_0">Value 0</option>
        <option value="opt_value_1">Value 1</option>
    </Field>
</Form>
```
</li>

<li>
<p><b>EnableDeviceSearcher <b></p>

When enabled, a search field is shown on the order page allowing the users to filter the devices. This can be removed when the number of available devices is limited by deleting or commenting the node.
```xml
<EnableDeviceSearcher />
```
</li>

<li>
<p><b>OrderFlow<b></p>

The OrderFlow node describe what are the different step to execute to get a request marked as completed. Each status node contains the following attributes that can optionally be set:

<ul>
<li><b>status</b>: Execute the function name defined into the user_functions.py file. This function shall return a string that is displayed into the admin interface. </li>
<li><b>action</b>: Execute the function name defined into the user_functions.py file. This function perform an action when the order goes to the associated state.</li>
</ul>&nbsp;

Additionally, an e-mail can be defined to be sent when the order goes to a state. For that, the `emailContent` node, which contain the `subject` and `HTML` content has to be set.

```xml
<OrderFlow>
    <Status status='user_status0_function' action='user_action0_function'> <!-- e.g.: action can be sendInvoice and status can be getInvoiceStatus -->
        <name>Status name</name>
        <emailContent>
            <subject>Email subject</subject>
            <HTML>
                <![CDATA[
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <title>Email subject</title>
                       </head>

                        <body>
                            <p>This is an example</p>
                            <p>Kind regards, <br/>PSMS Team</p>
                        </body>
                    </html>
                ]]>
            </HTML>
        </emailContent>
    </Status>

    <!-- other status... -->
</OrderFlow>
```
</li>

<li>
<p><b>Packages<b></p>

The Packages node list the supported kits, which can be ordered by the users. The Package node shall containt the following attributes:

<ul>
<li><b>indivualPurchase</b>: When set to true, the package is available for purchase.</li>
<li><b>vname</b>: Name of the package displayed in the user interface</li>
<li><b>dbname</b>: Name of the package used to link a package to a request. This name is used as reference and shall not be changed. It means that it is recommended to create a new package node, with a specific dbname, everytime the devices included change.</li>
</ul>&nbsp;

Then, a package node contains different sub-nodes used to list the devices included, define a package specific form that shall be filled-up, a description, a list of responsible and the price.

```xml
<Packages>

    <Package indivualPurchase="true" vname="Package name" dbname="Package_name_0">
        <Description>
            Package description, could be html using CDATA feature.
        </Description>

        <Devices>
            <Device dbname="devDbName0" qty="1" />
            <Device dbname="devDbName1" qty="1" />
            <Device dbname="devDbName2" qty="1" />
        </Devices>

        <Form>
            <Field name='expecteddate' type="date" verbose="Expected date"></Field>
            <!-- same structure as Form node -->
        </Form>

        <Responsible>
            <User informViaEmail="true">email.address@cern.ch</User> <!-- informViaEmail can be set to false if the use doesn't want to be informed of new orders -->
        </Responsible>

        <Prices>
            <UnitPrice fromQty="1">2000</UnitPrice>   <!-- price for quantity above 1 -->
            <UnitPrice fromQty="10">1800</UnitPrice>  <!-- price for quantity above 10 -->
            <UnitPrice fromQty="100">1700</UnitPrice> <!-- price for quantity above 100 -->
        </Prices>

    </Package>

    <!-- other packages... -->
</Packages>
```
</li>

<li>
<p><b>Devices<b></p>

The Devices node list the supported kits, which can be ordered by the users. The Device node shall containt the following attributes:

<ul>
<li><b>indivualPurchase</b>: When set to true, the device is available for purchase.</li>
<li><b>vname</b>: Name of the device displayed in the user interface</li>
<li><b>dbname</b>: Name of the device used to link a device to a request. This name is used as reference and shall not be changed. It means that it is recommended to create a new device node, with a specific dbname, everytime the devices included change.</li>
</ul>&nbsp;

Then, a device node contains different sub-nodes used to define a device specific form that shall be filled-up, a description, a list of responsible and the price.

```xml
<Devices>

    <Device indivualPurchase="true" vname="Device name" dbname="Package_name_0">
        <Description>
            Device description, could be html using CDATA feature.
        </Description>

        <Form>
            <Field name='expecteddate' type="date" verbose="Expected date"></Field>
            <!-- same structure as Form node -->
        </Form>

        <Responsible>
            <User informViaEmail="true">email.address@cern.ch</User> <!-- informViaEmail can be set to false if the use doesn't want to be informed of new orders -->
        </Responsible>

        <Flow> <!-- Define a specific flow to follow before considering the device to be ready -->
            <State name="Wait for stock" status='getStockStatus'> <!-- follow the same architecture as the OrderFlow node, except the email node (can contain status and action attributes) -->
                <selectDevices /> <!-- specify that device shall be selected when exiting this step (optional) -->
            </State>
            <State name="Completed" />
        </Flow>

        <Prices>
            <UnitPrice fromQty="1">2000</UnitPrice>   <!-- price for quantity above 1 -->
            <UnitPrice fromQty="10">1800</UnitPrice>  <!-- price for quantity above 10 -->
            <UnitPrice fromQty="100">1700</UnitPrice> <!-- price for quantity above 100 -->
        </Prices>

        <BatchDetails>  <!-- Defines some details to be set when registring a new batch -->
            <Field name='partnumber'>Part number</Field>
            <Field name='manufacturer'>Manufacturer</Field>
        </BatchDetails>

    </Device>

    <!-- other devices... -->
</Devices>
```
</li>
</ul>
