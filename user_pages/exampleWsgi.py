# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

import os
import pandas
import datetime
import json

import gevent
from gevent import pywsgi

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import stream_with_context
from flask import send_file
from flask import make_response
from flask import Blueprint

from pythonLib.psmsLib.DBIntf.prodDB import ProdDB

import pythonLib.psmsLib
import pythonLib.configmgt as webappconfig
import pythonLib.mailmgt as mailmgt
import pythonLib.usermgt as usermgt
import pythonLib.templatemgt as templatemgt

class ExampleWsgi:

    #View functions
    def __init__(self, application, database=None):
        application.add_url_rule('/', 'example', self.example)

    @usermgt.auth_required(level=0)
    def example(self):
        conf = webappconfig.WebAPPConfig()
        return templatemgt.gen('example_template.html', page_title="Example page")

exampleClass = None

def init(application, database=None):
    bp = Blueprint('example', __name__, url_prefix='/example', template_folder='./../templates')
    exampleClass = ExampleWsgi(bp, database)
    application.register_blueprint(bp)